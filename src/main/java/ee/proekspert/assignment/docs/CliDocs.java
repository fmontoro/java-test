package ee.proekspert.assignment.docs;



/**
 * 
 * Class used to show the docs with -h
 * @author Freddy Montoro
 *
 */
public class CliDocs {
	
	public static final String TAB ="\t";
	public static final String TOP_TITLE= TAB+"---Top---";
	public static final String HISTOGRAM_TITLE=TAB+"---Histogram---";
	public static final String ELAPSEDTIME_TITLE=TAB+"---Elapsed Time---"; 
	
	public String getDocs() {

		return this.getTitleDoc()+this.getTopDoc()+this.getHistogramDoc()+this.getTimeDoc()+this.getWololo();
	}
	
	private String getTitleDoc() {
		return System.lineSeparator()+TAB+"---Proekspert Java Assignment---"+System.lineSeparator()
				+ TAB+TAB+"No external libraries are used in this project"+System.lineSeparator()
				+ TAB+TAB+ "Arguments needed: a log file and a number. " +System.lineSeparator()
				+ TAB+TAB+ "i.e. foo.log 10. The number is used in Top"+System.lineSeparator();
	}
	
	
	private String getTopDoc() {
		return System.lineSeparator()+TOP_TITLE+System.lineSeparator()
				+ TAB+TAB+"Top average of requests based on resource based on second argument provided."+System.lineSeparator()
				+ TAB+TAB+"A group is based with the next logic: First a request is unique if it has a long/integer number"+System.lineSeparator()
				+ TAB+TAB+"as a value of an param, i.e. invoiceKey/invoiceId/msisd and also has an only CAPS"+System.lineSeparator()
				+ TAB+TAB+"value of another param. The group will be that CAPS value, i.e. SUBSCRIPTION/PHONECALL_DOMESTIC_PRICE."+System.lineSeparator()
				+ TAB+TAB+"Second, with the same logic of the long/integer number with the diference that an param is named from"+System.lineSeparator()
				+ TAB+TAB+"and has a value of another resource i.e. &from=customerInfo.do." + System.lineSeparator() 
				+ TAB+TAB+"Info is shown as resource, resouce-group (if it has one) and average";
	}
	
	private String getHistogramDoc() {
		return System.lineSeparator()+HISTOGRAM_TITLE+System.lineSeparator()
				+ TAB+TAB+"Total on the left side. Counter hour on the right side. Every 50 requests will show an asterik (*)";
	}
	
	private String getTimeDoc() {
		return System.lineSeparator()+ELAPSEDTIME_TITLE+System.lineSeparator()
				+ TAB+TAB+"Finally it shows the elapsed time in milliseconds";
	}
	
	private String getWololo() {	
		return System.lineSeparator()+"\t---Wololo---"+System.lineSeparator()
				+ TAB+TAB+"1337"+System.lineSeparator()
				+ TAB+TAB+"reading /do/wololo/play? in 1337 in the timing.log made me laugh";
	}
	
	
	

}
