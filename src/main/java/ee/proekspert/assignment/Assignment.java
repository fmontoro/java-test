package ee.proekspert.assignment;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.Collectors;

import ee.proekspert.assignment.docs.CliDocs;
import ee.proekspert.assignment.tasks.Histogram;
import ee.proekspert.assignment.tasks.Time;
import ee.proekspert.assignment.tasks.Top;
import ee.proekspert.assignment.tools.Arguments;


/**
 * 
 * Proekspert Java Assigment 
 * @author Freddy Montoro
 */
public class Assignment {

	
	public static void main(String[] args) throws IOException {


	    Top top = new Top();
		Histogram histogram = new Histogram();
		Time time = new Time();
		time.setInitialTime();
		int counterDefault=10;
		if(new Arguments().validate(args)) {
			
			try {
		    	top.setTopArgument(Integer.valueOf(args[1]));
		    }catch(Exception e) {
		    	System.out.println("Second argument is too big. Default is 10");
		    	top.setTopArgument(counterDefault);	
		    }
			
		}else{
	    	top.setTopArgument(counterDefault);
		};
		
		FileReader fileReader = new FileReader(args[0]);
	    BufferedReader bufferedReader = new BufferedReader(fileReader);
	    String line="";
	    
	    while ((line = bufferedReader.readLine()) != null) {
	    	top.setCurrentLine(line);
	    	histogram.setCurrentLine(line);			
	    }
	    
	    top.show();
		histogram.show();
		
		time.setFinalTime();
		System.out.println("\n"+CliDocs.ELAPSEDTIME_TITLE);
		System.out.println(time.calcElapsedTime() + " milliseconds");
		
		

	}// main
}
