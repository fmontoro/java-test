package ee.proekspert.assignment.tools;

import java.io.File;

import ee.proekspert.assignment.docs.CliDocs;

public class Arguments {

	private void exitProgram(String message) {
		System.out.println(message);
		System.exit(0);
	}

	public boolean validate(String[] args) {

		switch (args.length) {
		case 1: {
			if (args[0].equals("-h")) {
				CliDocs cliDocs = new CliDocs();
				this.exitProgram(cliDocs.getDocs());

			} else {
				return false;
			}

			break;
		}

		case 2: {

			String ext = args[0].substring(args[0].lastIndexOf(".") + 1, args[0].length());
			if (ext.equals("log") && new File(args[0]).exists()) {

				try {
					if (args[1].matches("\\d+")) {
						return true;
					} else {
						this.exitProgram("Second argument not valid.Needs to be a number. Use -h for help");
					}

				} catch (Exception e) {
					this.exitProgram("Second argument not valid. Use -h for help");
				}

			} else {
				this.exitProgram("Error: file not valid or file not found");
			}

			break;
		}

		default: {
			this.exitProgram("No arguments found. Use -h for help");
		}
		}

		return false;
	}

}
