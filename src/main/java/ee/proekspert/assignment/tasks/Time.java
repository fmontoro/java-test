package ee.proekspert.assignment.tasks;


/**
 * 
 * Class used to show the result of point #3 
 * @author Freddy Montoro
 *
 */
public class Time {

	private long initialTime = 0;
	private long finalTime = 0;

	
	/**
	 * Calculates the elapsed time of the program
	 * @return elapsed time
	 */
	public long calcElapsedTime() {
		return this.getFinalTime() - this.getInitialTime();
	}
	
	//Boilerplate code
	
	public long getInitialTime() {
		return initialTime;
	}

	public void setInitialTime() {
		this.initialTime = System.currentTimeMillis();
	}

	public long getFinalTime() {
		return finalTime;
	}

	public void setFinalTime() {
		this.finalTime = System.currentTimeMillis();
	}

	@Override
	public String toString() {
		return "Time [initialTime=" + initialTime + ", finalTime=" + finalTime +  "]";
	}

}
