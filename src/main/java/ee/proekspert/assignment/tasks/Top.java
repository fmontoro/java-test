package ee.proekspert.assignment.tasks;

import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import ee.proekspert.assignment.docs.CliDocs;

/**
 * 
 * Class used to show the result of point #1
 * 
 * @author Freddy Montoro
 *
 */
public class Top {
	private int topArgument = 0;
	private String currentLine = "";
	private HashMap<String, String[]> averageMap = new HashMap<String, String[]>();
	private HashMap<String, Double> hm = new HashMap<String, Double>();
	private int MAX_LENGTH = 0;

	private Matcher match(String regex) {
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(this.getCurrentLine());
		return matcher;

	}

	private String getGroup() {
		Matcher groupA = this.match("\\=[A-Z_]+([A-Z_\\d]+)?");
		Matcher groupB = this.match("=[\\w]+.do");
		Matcher id = this.match("\\=[\\d]+");

		if (groupA.find() && id.find()) {			
			return groupA.group(0).substring(1, groupA.group(0).length());
		} else if (groupB.find() && id.find()) {
			return groupB.group(0).substring(1, groupB.group(0).length());
		}

		return "";

	}

	private long getTimeRequest() {
		return Long.valueOf(this.getCurrentLine().split("\\sin\\s")[1]);
	}

	private void setAverageMap(String resource, int counter, Long timeRequest) {
		String group = "";
		try {
			group = resource.split("-")[1];

			if (group.charAt(group.length() - 1) == '_') {
				resource = resource.split("-")[0];
			}

		} catch (Exception e) {
			// split error group empty
		}

		String[] values = { resource, String.valueOf(timeRequest), String.valueOf(counter + 1), group };
		this.averageMap.put(resource, values);

	}

	public void set() {

		String group = this.getGroup();
		String resource = this.getCurrentLine().split(" ")[4];
		if (resource.indexOf("?") > -1) {
			resource = resource.substring(0, resource.indexOf("?"));
		} else if (resource.contains("&") && !resource.contains("?")) {
			resource = resource.substring(0, resource.indexOf("&"));
		}

		boolean done = (!group.equals("")) ? this.setMap(resource + "-" + group) : this.setMap(resource);
	}

	private boolean setMap(String resource) {
		int counter = 0;
		Long timeRequest = this.getTimeRequest();
		try {
			counter = Integer.valueOf(this.averageMap.get(resource)[2]);
			timeRequest += Long.valueOf(this.averageMap.get(resource)[1]);

			this.setAverageMap(resource, counter, timeRequest);
		} catch (Exception e) {
			this.setAverageMap(resource, counter, timeRequest);
		}

		return true;
	}

	public void show() {

		int counter = 0;
		
		//Get average of each resource
		this.averageMap.forEach((k, v) -> {
			Double average = Double.valueOf(v[1]) / Double.valueOf(v[2]);
			this.hm.put(k, average);

		});

		
		if (this.getTopArgument() > hm.size()) {
			System.out.println("Elements to show in top are smaller than the list");
			System.exit(0);
		}

		//Sort final hashmap in reverse order
		this.hm = this.hm.entrySet().stream().sorted(Collections.reverseOrder(Map.Entry.comparingByValue()))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (key, value) -> value, LinkedHashMap::new));

		//Set value of second argument taken from terminal
		List<Map.Entry<String, Double>> list = this.hm.entrySet().stream().limit(this.getTopArgument())
				.collect(Collectors.toList());

		System.out.println("\n" + CliDocs.TOP_TITLE);
		
		//Set maxlength of chars to be used as gap
		for (Map.Entry<String, Double> entry : list) {
			if (this.MAX_LENGTH < entry.getKey().length()) {
				this.MAX_LENGTH = entry.getKey().length();
			}
		}
		
		
		//Displays final data
		for (Map.Entry<String, Double> entry : list) {
			int g = this.MAX_LENGTH - entry.getKey().length();
			String gap = "";
			for (int i = 0; i < g; i++) {
				gap += " ";
			}

			counter++;
			System.out.printf(counter + ".\t " + entry.getKey() + gap + " |\t  %.2f\n", entry.getValue());
		}
	}

	// Boilerplate
	public long getTopArgument() {
		return topArgument;
	}

	public void setTopArgument(int topArgument) {
		this.topArgument = topArgument;
	}

	public String getCurrentLine() {
		return currentLine;
	}

	public void setCurrentLine(String currentLine) {
		this.currentLine = currentLine;
		this.set();
	}

}
