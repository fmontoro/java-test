package ee.proekspert.assignment.tasks;

import java.util.HashMap;

import ee.proekspert.assignment.docs.CliDocs;

/**
 * 
 * Class used to show the result of point #2
 * 
 * @author Freddy Montoro
 *
 */
public class Histogram {

	private String currentLine = "";
	private int maxTotalRequests = 0;

	private HashMap<String, Long> dataMap = new HashMap<String, Long>();

	private void data() {

		String time = this.getCurrentLine().split("\\s")[1].split(":")[1];

		try {
			this.getDataMap().put(time, this.getDataMap().get(time) + 1);
		} catch (Exception e) {
			this.getDataMap().put(time, 1L);

		}

	}

	private String makeChars(String ch, long limit, long increase) {
		String result = "";
		for (long i = 0; i < limit; i = i + increase) {
			result += ch;
		}

		return result;
	}

	public void show() {
		System.out.println(
				System.lineSeparator() + System.lineSeparator() + CliDocs.HISTOGRAM_TITLE + System.lineSeparator());

		this.getDataMap().forEach((k, v) -> {
			String stars = this.makeChars("*", v, 50);
			System.out.println(k + " | " + v + " :" + stars);

		});
	}

	public String getCurrentLine() {
		return currentLine;
	}

	public void setCurrentLine(String currentLine) {
		this.currentLine = currentLine;
		this.data();
	}

	public int getMaxTotalRequests() {
		return maxTotalRequests;
	}

	public void setMaxTotalRequests(int maxTotalRequests) {
		this.maxTotalRequests = maxTotalRequests;
	}

	public HashMap<String, Long> getDataMap() {
		return dataMap;
	}

	public void setDataMap(HashMap<String, Long> dataMap) {
		this.dataMap = dataMap;
	}

}
